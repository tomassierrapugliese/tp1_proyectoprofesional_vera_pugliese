DROP DATABASE IF EXISTS `grupo_10`;
CREATE DATABASE `grupo_10`;
USE `grupo_10`;
CREATE TABLE `personas`
(
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Telefono` varchar(20) NOT NULL,
  `fechaNacimiento` date NOT NULL, 
  `email` varchar (35) NOT NULL,
  PRIMARY KEY (`idPersona`)
);

SET GLOBAL time_zone = '+3:00';

SELECT * FROM personas