package dto;

import java.time.LocalDate;

public class PersonaDTO 
{
	private int idPersona;
	private String nombre;
	private String telefono;
	private LocalDate fechaNacimiento;
	private String email;


	public PersonaDTO(int idPersona, String nombre, String telefono, LocalDate fecha, String email)
	{
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.telefono = telefono;
		this.fechaNacimiento = fecha;
		this.email = email;
	}
	
	public int getIdPersona() 
	{
		return this.idPersona;
	}

	public void setIdPersona(int idPersona) 
	{
		this.idPersona = idPersona;
	}

	public String getNombre() 
	{
		return this.nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public String getTelefono() 
	{
		return this.telefono;
	}

	public void setTelefono(String telefono) 
	{
		this.telefono = telefono;
	}

	public String getEmail() 
	{
		return this.email;
	}
	
	public void setEmail(String email) 
	{
		this.email = email;
	}
	
	public LocalDate getFechaNacimiento() 
	{
		return fechaNacimiento;
	}
	
	public void setFechaNacimiento(LocalDate fechaNacimiento) 
	{
		this.fechaNacimiento = fechaNacimiento;
	}
}
